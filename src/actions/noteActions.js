export const getNotes = () => (dispatch) => {
  fetch(`http://localhost:8080/api/posts`)
    .then(response => response.json())
    .then(result => {
      dispatch({
        type: 'GET_NOTES',
        notes: result
      })
    })
};

export const deleteNotes = (props) => (dispatch) => {
  fetch(`http://localhost:8080/api/posts/${props.match.params.id}`,
    {method: "delete"})
    .then(() => {dispatch ({
        type: 'DELETE_NOTES',
      });
      props.history.push('/')
    });
};

export const setCaption = (caption) => {
  return {
   type: 'SET_CAPTION',
   caption: caption
  }
};

export const setContent = (content) => {
  return {
    type: 'SET_CONTENT',
    content: content
  }
};