import React, {Component} from 'react';
import Header from "../components/Header";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {setCaption, setContent} from "../actions/noteActions";
import {connect} from "react-redux";

class CreateNote extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleCaptionChange = this.handleCaptionChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <div>
        <Header/>
        <main>
          <h1>创建笔记</h1>
          <hr/>
          <label>
            <p>标题</p>
            <input onChange={this.handleCaptionChange} type="text"/>
          </label>
          <label>
            <p>正文</p>
            <textarea onChange={this.handleContentChange} name="content" id="note-content" cols="30" rows="10"/>
          </label>
          <div>
            <button disabled={(!this.props.content.trim())||(!this.props.caption.trim())} onClick={this.handleSubmit}>提交</button>
            <Link to='/'><button>取消</button></Link>
          </div>
        </main>
      </div>
    );
  }

  handleCaptionChange(e) {
    this.props.setCaption(e.target.value);
  }

  handleContentChange(e) {
    this.props.setContent(e.target.value);
  }

  handleSubmit() {
    const request = {title: this.props.caption, description: this.props.content};
    const string = JSON.stringify(request);
    fetch("http://localhost:8080/api/posts",
      {method:"POST", body: string, headers: {'Content-Type': 'application/json'}})
      .then(this.props.history.push('/'));
  }
}

const mapStateToProps = state => ({
  caption: state.noteReducer.caption,
  content: state.noteReducer.content
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setCaption, setContent
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateNote);
