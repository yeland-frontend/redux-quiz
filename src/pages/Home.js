import React from "react";
import HomeNote from "../components/HomeNote";
import {MdQueue} from "react-icons/md";
import {getNotes, setCaption, setContent} from "../actions/noteActions";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Header from "../components/Header";

class Home extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getNotes();
    this.props.setCaption('');
    this.props.setContent('');
  }

  render() {
    const notes = this.props.notes;
    return (
      <div>
        <Header />
        {notes.map(note => <HomeNote title={note.title} id={note.id}/>)}
        <HomeNote title={<MdQueue/>} id="create"/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  notes: state.noteReducer.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getNotes, setCaption, setContent
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);