import React, {Component} from 'react';
import Header from "../components/Header";
import HomeNote from "../components/HomeNote";
import {bindActionCreators} from "redux";
import {deleteNotes, getNotes} from "../actions/noteActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

class NoteDetails extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.props.getNotes();
  }

  render() {
    const notes = this.props.notes;
    const id = this.props.match.params.id;
    const note = notes.filter(note => note.id == id)[0];
    return (
      <div>
        <Header />
        <nav>
          {notes.map(note => <HomeNote title={note.title} id={note.id}/>)}
        </nav>
        <main>
          <h1>{note.title}</h1>
          <p>{note.description}</p>
        </main>
        <footer>
          <button onClick={this.handleClick}>删除</button>
          <Link to='/'><button>返回</button></Link>
        </footer>
      </div>
    );
  }

  handleClick() {
    this.props.deleteNotes(this.props);
  }
}

const mapStateToProps = state => ({
  notes: state.noteReducer.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getNotes, deleteNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetails);