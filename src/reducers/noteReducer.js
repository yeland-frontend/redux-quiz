const initState = {
  notes: [{}]
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_NOTES':
      return {
        ...state,
        notes: action.notes
      };
    case 'DELETE_NOTES':
      return {
        ...state
      };
    case 'SET_CAPTION':
      return {
        ...state,
        caption: action.caption
      };
    case 'SET_CONTENT':
      return {
        ...state,
        content: action.content
      };
    default:
      return state
  }
};
