import React from 'react';
import noteReducer from "../noteReducer";

test('执行获取note时返回新的state', () => {
  const action = {
    type: 'GET_NOTES',
    notes: {
      id: 1,
      title: 'CSS'
    }
  };

  const actualState = noteReducer(undefined, action);
  const expectState = {
    notes: {
      id: 1,
      title: 'CSS'
    }
  };

  expect(actualState).toEqual(expectState);
});

test('执行设置caption时返回新的state', () => {
  const action = {
    type: 'SET_CAPTION',
    caption: 'hello'
  };

  const actualState = noteReducer(undefined, action);
  const expectState = {
    caption: 'hello',
    notes: [{}]
  };

  expect(actualState).toEqual(expectState);
});

test('执行设置content时返回新的state', () => {
  const action = {
    type: 'SET_CONTENT',
    content: 'world'
  };

  const actualState = noteReducer(undefined, action);
  const expectState = {
    content:'world',
    notes: [{}]
  };

  expect(actualState).toEqual(expectState);
});
