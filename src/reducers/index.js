import {combineReducers} from "redux";
import noteReducer from "./noteReducer";

const reducers = combineReducers({
  noteReducer
});
export default reducers;