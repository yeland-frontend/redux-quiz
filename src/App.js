import React, {Component} from 'react';
import './App.less';
import Home from "./pages/Home";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import NoteDetails from "./pages/NoteDetails";
import CreateNote from "./pages/CreateNote";

class App extends Component {
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/notes/:id(\d+)" component={NoteDetails}/>
            <Route path="/notes/create" component={CreateNote}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;