import React from "react";
import {MdAssignment} from "react-icons/md";

const Header = () => {
  return (
    <header>
      <MdAssignment className="assignment-icon"/>
      <span>NOTES</span>
    </header>
  )
};

export default Header;