import React from "react";
import {Link} from "react-router-dom";

const HomeNote = (props) => {
  return (
    <li>
      <Link to={`/notes/${props.id}`}>
        <div>
          {props.title}
        </div>
      </Link>
    </li>
  )
};

export default HomeNote;